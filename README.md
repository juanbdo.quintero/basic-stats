Basic Stats
===========

The challenge is to create a program that computes some basic statistics on a collection of small positive integers. You can assume all values will be less than 1,000.
<br>
The DataCapture object accepts numbers and returns an object for querying statistics about the inputs. Specifically, the returned object supports querying how many numbers in the collection are less than a value, greater than a value, or within a range.
<br>

Conditions:
* The methods add(), less(), greater(), and between() should have
constant time O(1).
* The method build_stats() can be at most linear O(n).

## Usage:

Make sure you have installed [Python3+](https://www.python.org/) and:

```console
git clone https://gitlab.com/juanbdo.quintero/basic-stats.git
cd basic-stats
python main.py
```
or
```console
python3 main.py
```

## Test

Install [Pytest](https://docs.pytest.org/en/7.1.x/) :

```console
pip install pytest==7.1.2
```
run tests
```console
cd tests
pytest
```
To add another UT edit the file *tests/test_data_capture* and add a test case to the *test_cases* variable: each test case is a tuple with a list of numbers representing the collection and a dictionary containing the method calls and the expected values.